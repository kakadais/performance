cl = console.log

@test = ->
  db.remove({})

  start = new Date()

  count = 10000

  for i in [0...count]
    db.insert idx: i

  cl '...'

  for i in [0...count]
    db.insert {
      "createdAt" : new Date(),
      "updatedAt" : new Date(),
      "년월일" : "2020-06-09",
      "connectionInfo" : {
        "id" : "WsocqDnsmbefRShDq",
        "clientAddress" : "180.67.202.221",
        "httpHeaders" : {
          "referer" : "https://test.kr/childrenGroup",
          "x-forwarded-for" : "180.67.202.221,127.0.0.1",
          "x-forwarded-host" : "localhost:5020",
          "x-forwarded-port" : "5020",
          "x-real-ip" : "180.67.202.221",
          "x-forwarded-proto" : "http",
          "host" : "localhost:5020",
          "user-agent" : "Mozilla/5.0 (Linux; Android 10; SM-G981N Build/QP1A.190711.020; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/81.0.4044.138 Mobile Safari/537.36 [iwtJamsuneApp 2.2.3]",
          "accept-language" : "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7"
        }
      }
    }

  db.remove({})
  end = new Date()
  cl end - start

test()

